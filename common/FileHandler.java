package common;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;

public class FileHandler {
    /**
     * Reads a file and returns an ArrayList of Strings.
     * 
     * @param fileName The name of the file to read.
     * @return An ArrayList of Strings.
     */
    public static ArrayList<String> readFile(String fileName) {
        ArrayList<String> lines = new ArrayList<String>();
        try (Scanner scanner = new Scanner(new FileReader(fileName));) {
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }
            scanner.close();
        } catch (IOException e) {
            System.out.println("Error reading file: " + fileName);
        }
        return lines;
    }
}
